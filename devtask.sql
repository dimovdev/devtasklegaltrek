-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия на сървъра:            10.1.37-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win32
-- HeidiSQL Версия:              10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дъмп на структурата на БД devtask
CREATE DATABASE IF NOT EXISTS `devtask` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `devtask`;

-- Дъмп структура за таблица devtask.cities
CREATE TABLE IF NOT EXISTS `cities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дъмп данни за таблица devtask.cities: ~30 rows (приблизително)
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
REPLACE INTO `cities` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Sofia', NULL, NULL),
	(2, 'Plovdiv', NULL, NULL),
	(3, 'Varna', NULL, NULL),
	(4, 'Burgas', NULL, NULL),
	(5, 'Ruse', NULL, NULL),
	(6, 'Stara Zagora', NULL, NULL),
	(7, 'Pleven', NULL, NULL),
	(8, 'Sliven', NULL, NULL),
	(9, 'Dobrich', NULL, NULL),
	(10, 'Shumen', NULL, NULL),
	(11, 'Pernik', NULL, NULL),
	(12, 'Haskovo', NULL, NULL),
	(13, 'Vratsa', NULL, NULL),
	(14, 'Kyustendil', NULL, NULL),
	(15, 'Plovdiv', NULL, NULL),
	(16, 'Montana', NULL, NULL),
	(17, 'Lovech', NULL, NULL),
	(18, 'Razgrad', NULL, NULL),
	(19, 'Borino', NULL, NULL),
	(20, 'Madan', NULL, NULL),
	(21, 'Zlatograd', NULL, NULL),
	(22, 'Pazardzhik', NULL, NULL),
	(23, 'Smolyan', NULL, NULL),
	(24, 'Blagoevgrad', NULL, NULL),
	(25, 'Nedelino', NULL, NULL),
	(26, 'Rudozem', NULL, NULL),
	(27, 'Veliko Tarno', NULL, NULL),
	(28, 'Devin', NULL, NULL),
	(29, 'Vidin', NULL, NULL),
	(30, 'Krumovgrad', NULL, NULL);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;

-- Дъмп структура за таблица devtask.city_day
CREATE TABLE IF NOT EXISTS `city_day` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city_id` int(10) unsigned NOT NULL,
  `day_id` int(10) unsigned NOT NULL,
  `min_temp` int(11) NOT NULL,
  `max_temp` int(11) NOT NULL,
  `precipitation` int(11) NOT NULL,
  `humidity` int(11) NOT NULL,
  `wind` int(11) NOT NULL,
  `current_temp` int(11) NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дъмп данни за таблица devtask.city_day: ~70 rows (приблизително)
/*!40000 ALTER TABLE `city_day` DISABLE KEYS */;
REPLACE INTO `city_day` (`id`, `city_id`, `day_id`, `min_temp`, `max_temp`, `precipitation`, `humidity`, `wind`, `current_temp`, `type`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 13, 25, 63, 12, 9, 5, 'cloudy', NULL, NULL),
	(2, 1, 2, 21, 22, 43, 59, 83, 10, 'rain', NULL, NULL),
	(3, 1, 3, 4, 4, 28, 68, 6, 8, 'cloudy', NULL, NULL),
	(4, 1, 4, 19, 19, 10, 69, 69, 7, 'cloudy', NULL, NULL),
	(5, 1, 5, 23, 18, 42, 42, 40, 13, 'rain', NULL, NULL),
	(6, 1, 6, 28, 4, 27, 7, 89, 13, 'cloudy', NULL, NULL),
	(7, 1, 7, 10, 26, 13, 4, 36, 27, 'cloudy', NULL, NULL),
	(9, 2, 1, 26, 28, 59, 1, 10, 2, 'cloudy', NULL, NULL),
	(10, 2, 2, 8, 1, 28, 88, 38, 6, 'cloudy', NULL, NULL),
	(11, 2, 3, 24, 15, 39, 53, 65, 14, 'cloudy', NULL, NULL),
	(12, 2, 4, 4, 9, 34, 1, 21, 4, 'cloudy', NULL, NULL),
	(13, 2, 5, 10, 30, 55, 37, 5, 9, 'cloudy', NULL, NULL),
	(14, 2, 6, 7, 3, 23, 89, 57, 3, 'rain', NULL, NULL),
	(15, 2, 7, 6, 15, 25, 45, 80, 20, 'cloudy', NULL, NULL),
	(16, 3, 1, 10, 6, 58, 57, 37, 29, 'cloudy', NULL, NULL),
	(17, 3, 2, 2, 17, 61, 54, 41, 22, 'cloudy', NULL, NULL),
	(18, 3, 3, 13, 7, 58, 1, 0, 24, 'cloudy', NULL, NULL),
	(19, 3, 4, 26, 13, 32, 38, 68, 25, 'cloudy', NULL, NULL),
	(20, 3, 5, 29, 15, 59, 92, 53, 23, 'cloudy', NULL, NULL),
	(21, 3, 6, 5, 6, 51, 57, 61, 10, 'cloudy', NULL, NULL),
	(22, 3, 7, 1, 18, 2, 8, 51, 13, 'rain', NULL, NULL),
	(23, 4, 1, 24, 7, 10, 62, 72, 5, 'cloudy', NULL, NULL),
	(24, 4, 2, 22, 14, 45, 1, 14, 14, 'cloudy', NULL, NULL),
	(25, 4, 3, 9, 18, 45, 11, 49, 28, 'cloudy', NULL, NULL),
	(26, 4, 4, 10, 16, 11, 52, 10, 4, 'cloudy', NULL, NULL),
	(27, 4, 5, 24, 27, 0, 35, 94, 0, 'cloudy', NULL, NULL),
	(28, 4, 6, 28, 25, 41, 19, 58, 17, 'cloudy', NULL, NULL),
	(29, 4, 7, 9, 13, 53, 86, 7, 25, 'cloudy', NULL, NULL),
	(30, 5, 1, 20, 24, 69, 85, 55, 5, 'cloudy', NULL, NULL),
	(31, 5, 2, 14, 19, 34, 74, 62, 10, 'rain', NULL, NULL),
	(32, 5, 3, 11, 21, 40, 20, 50, 8, 'cloudy', NULL, NULL),
	(33, 5, 4, 11, 20, 21, 70, 66, 7, 'cloudy', NULL, NULL),
	(34, 5, 5, 22, 4, 63, 2, 83, 13, 'cloudy', NULL, NULL),
	(35, 5, 6, 17, 26, 23, 89, 28, 13, 'cloudy', NULL, NULL),
	(36, 5, 7, 19, 23, 4, 57, 81, 27, 'cloudy', NULL, NULL),
	(37, 6, 1, 15, 7, 27, 17, 35, 5, 'cloudy', NULL, NULL),
	(38, 6, 2, 16, 26, 48, 10, 26, 10, 'cloudy', NULL, NULL),
	(39, 6, 3, 7, 20, 5, 0, 27, 8, 'cloudy', NULL, NULL),
	(40, 6, 4, 16, 23, 37, 67, 57, 7, 'cloudy', NULL, NULL),
	(41, 6, 5, 27, 24, 17, 48, 14, 13, 'rain', NULL, NULL),
	(42, 6, 6, 29, 22, 52, 37, 89, 13, 'cloudy', NULL, NULL),
	(43, 6, 7, 2, 5, 56, 44, 22, 27, 'cloudy', NULL, NULL),
	(44, 7, 1, 14, 21, 47, 13, 34, 5, 'cloudy', NULL, NULL),
	(45, 7, 2, 5, 29, 69, 31, 9, 10, 'rain', NULL, NULL),
	(46, 7, 3, 14, 20, 54, 17, 39, 8, 'cloudy', NULL, NULL),
	(47, 7, 4, 25, 15, 64, 91, 72, 7, 'cloudy', NULL, NULL),
	(48, 7, 5, 22, 13, 4, 21, 53, 13, 'cloudy', NULL, NULL),
	(49, 7, 6, 4, 24, 59, 24, 49, 13, 'cloudy', NULL, NULL),
	(50, 7, 7, 16, 17, 55, 59, 87, 27, 'cloudy', NULL, NULL),
	(51, 8, 1, 5, 13, 23, 31, 94, 5, 'cloudy', NULL, NULL),
	(52, 8, 2, 8, 15, 26, 72, 19, 10, 'cloudy', NULL, NULL),
	(53, 8, 3, 27, 4, 61, 77, 5, 8, 'cloudy', NULL, NULL),
	(54, 8, 4, 19, 10, 75, 72, 67, 7, 'cloudy', NULL, NULL),
	(55, 8, 5, 15, 5, 42, 36, 31, 13, 'cloudy', NULL, NULL),
	(56, 8, 6, 17, 28, 62, 61, 50, 13, 'cloudy', NULL, NULL),
	(57, 8, 7, 10, 1, 33, 3, 61, 27, 'cloudy', NULL, NULL),
	(58, 9, 1, 0, 14, 55, 26, 58, 5, 'cloudy', NULL, NULL),
	(59, 9, 2, 3, 5, 26, 27, 15, 10, 'cloudy', NULL, NULL),
	(60, 9, 3, 15, 13, 40, 55, 90, 8, 'cloudy', NULL, NULL),
	(61, 9, 4, 3, 21, 47, 1, 24, 7, 'cloudy', NULL, NULL),
	(62, 9, 5, 1, 6, 39, 35, 41, 13, 'cloudy', NULL, NULL),
	(63, 9, 6, 30, 30, 57, 77, 37, 13, 'cloudy', NULL, NULL),
	(64, 9, 7, 23, 7, 13, 90, 65, 27, 'rain', NULL, NULL),
	(65, 10, 1, 26, 6, 49, 24, 22, 5, 'cloudy', NULL, NULL),
	(66, 10, 2, 29, 9, 52, 43, 11, 10, 'cloudy', NULL, NULL),
	(67, 10, 3, 6, 0, 37, 50, 87, 8, 'cloudy', NULL, NULL),
	(68, 10, 4, 7, 1, 31, 24, 18, 7, 'cloudy', NULL, NULL),
	(69, 10, 5, 16, 7, 44, 65, 20, 13, 'cloudy', NULL, NULL),
	(70, 10, 6, 29, 2, 52, 62, 48, 13, 'cloudy', NULL, NULL),
	(71, 10, 7, 4, 20, 54, 20, 83, 27, 'cloudy', NULL, NULL);
/*!40000 ALTER TABLE `city_day` ENABLE KEYS */;

-- Дъмп структура за таблица devtask.days
CREATE TABLE IF NOT EXISTS `days` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дъмп данни за таблица devtask.days: ~7 rows (приблизително)
/*!40000 ALTER TABLE `days` DISABLE KEYS */;
REPLACE INTO `days` (`id`, `day`, `created_at`, `updated_at`) VALUES
	(1, 'sun', '2019-12-14 10:54:46', NULL),
	(2, 'mon', '2019-12-14 10:54:44', NULL),
	(3, 'tue', '2019-12-14 10:54:44', NULL),
	(4, 'wed', '2019-12-14 10:54:43', NULL),
	(5, 'thur', '2019-12-14 10:54:42', NULL),
	(6, 'fri', '2019-12-14 10:54:41', NULL),
	(7, 'sat', '2019-12-14 10:54:40', NULL);
/*!40000 ALTER TABLE `days` ENABLE KEYS */;

-- Дъмп структура за таблица devtask.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дъмп данни за таблица devtask.failed_jobs: ~0 rows (приблизително)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Дъмп структура за таблица devtask.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дъмп данни за таблица devtask.migrations: ~6 rows (приблизително)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
REPLACE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_12_13_160531_create_cities_table', 1),
	(5, '2019_12_13_200052_create_days_table', 1),
	(6, '2019_12_13_202048_create_cities_days_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Дъмп структура за таблица devtask.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дъмп данни за таблица devtask.password_resets: ~0 rows (приблизително)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Дъмп структура за таблица devtask.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дъмп данни за таблица devtask.users: ~0 rows (приблизително)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
