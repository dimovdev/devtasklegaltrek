<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html>
<head>
    <title>Sunlight Weather Widget Flat Responsive Widget Template :: w3layouts</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Sunlight Weather Widget Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!-- //for-mobile-apps -->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href='//fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css'>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
</head>
<body>
<style>
    #favourite_city li {
        display: inline-block;
        hover: background-color: white;
    }
</style>

<!-- main -->
<div class="main">
    <div style="absolute">
        <select name="city" id="city">
            <option selected disabled value="all">{{__('Select city')}}</option>
            @foreach($cities as $c)
            <option value="{{$c['id']}}" id="city-name">{{$c['name']}}</option>

            @endforeach
        </select>
        <button id="favourite" style="background: white;border:none">{{__('Add to favourite')}}</button>
        <button id="C" class="C" data-value="C" style="background: white;border:none">°C</button>
        <button id="F" class="F" data-value="F" style="background: white;border:none">°F</button>
        <a href="/locale/en" role="button" style="background: white;border:none">EN</a>
        <a href="/locale/bg" role="button" style="background: white;border:none">BG</a>
        <p></p>
        <span>{{__('My cities')}}<ul class="button" style="border:none" id="favourite_city" ></ul></span>

        <!--        loading partial via ajax-->
        <div class="partial">
        </div>
    </div>

    <script>

        $('#city').on('change', function (e) {
            callPartial();
        });

        function callPartial(tempType) {
            tempType = tempType || 'C';
            $.ajax({
                url: "/change_city",
                type: "POST",
                data: {
                    '_token': "{{ csrf_token() }}",
                    selected: $('#city').val(),
                    tempType: tempType
                }
            }).done(function (res) {
                $('.partial').html(res);

            })
        }

        callPartial();

        $('#favourite').on('click', function () {
            $.ajax({
                url: "/add_favourite",
                type: "POST",
                data: {
                    city_id: $('#city').val(),
                    '_token': "{{ csrf_token() }}"
                }
            }).done(function (res) {
                console.log(res)
                getFavourites();
            })
        })
        getFavourites();

        function getFavourites() {
            $.ajax({
                url: "/get_favourite",
                type: "POST",
                data: {
                    '_token': "{{ csrf_token() }}"
                }
            }).done(function (res) {

                var fav_city = $.parseJSON(res);
                $('#favourite_city').html('');
                $.each(fav_city, function (city_id, city_name) {
                    $('#favourite_city').append("<li><span  onclick='changeCity(" + city_id + ")'>" + city_name + "</span> <span onclick='removeCity(" + city_id + ",this)'>X</span></li>");
                })
            })
        }

        function changeCity(city_id) {
            $('#city').val(city_id).trigger('change');
        }

        function removeCity(city_id, el) {
            $(el).parents('li').remove();
            $.ajax({
                url: "/forget_cookie",
                type: "POST",
                data: {

                    city_id: city_id,
                    '_token': "{{ csrf_token() }}"
                }
            }).done(function (res) {
                console.log(res)

            })
        }

        $(document).on('click', '.C,.F', function () {
            var value = $(this).data('value');
            callPartial(value);


        })


    </script>
</body>
</html>

