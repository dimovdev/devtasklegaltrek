<h1>Sunlight Weather Widget</h1>

<div class="main-grid1">
    <div class="main-grid1-pos">
          <span>
         </span>
    </div>

    <h2 class="temp" id="temp" >{{$data[$city->id]['current_temp']}}<sup class="degree"></sup><span class="scale">°{{$tempType}}</span></h2>
    <p id="selected-name">{{$city->name}}</p>
    <div class="main-grid1-grids">
        <div class="main-grid1-grid-left">
              <span><img src="images/{{$data[$city->id]['type']}}.png" alt=" " class="img-responsive"/></span>
            <p>{{$data[$city->id]['max_temp']}}<sup class="degree">°{{$tempType}}</sup> {{$data[$city->id]['min_temp']}}<sup class="degree">°{{$tempType}}</sup></p>
            <h3>{{$data[$city->id]['type']}}</h3>
        </div>
        <div class="main-grid1-grid-right">

            <p><span>Humidity:{{$data[$city->id]['humidity']}}%</span><span>Precipitation:{{$data[$city->id]['precipitation']}}%</span><span>Wind:{{$data[$city->id]['precipitation']}} km/h</span><span>{{ date('H:i') }}</span>{{ date('D M Y') }}</p>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div class="main-grid2">
    @foreach($data as $key=>$day)
    <div class="main-grid2-left">
        <img src="images/{{$day->type}}.png" alt=" " class="img-responsive"/>
        <p>{{$days[$key]['day']}} <span>{{$data[$key]['max_temp'] }} °{{$tempType}} / {{$data[$key]['min_temp'] }} °{{$tempType}}<sup
                    class="degree"></sup></span></p>
    </div>
    @endforeach

    <div class="clear"></div>
</div>
<div class="footer">
    <p>Copyright © 2015 Sunlight Weather Widget. All rights reserved | Design by <a
            href="http://w3layouts.com">W3layouts</a>
    </p>
</div>
