<?php

namespace App\Http\Controllers;

use App\City;
use App\Day;
use Illuminate\Support\Facades\DB;
use Stevebauman\Location\Facades\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CityController extends Controller
{
    public function index(Request $request)
    {
        $ip = request()->ip();

        $data = Location::get('77.85.109.88');

        $cityName = $data->cityName;
        $cityId = DB::table('cities')->where('name', $cityName)->get()->toArray();
        $id = $cityId[0]->id;

        if (isset($request->selected)) {
            $id = $request->selected;
        }

        $cities = City::all();

        $city = City::find($id);

        $days = Day::get();

        $data = [];
        foreach ($city->days as $prop) {
            $data[] = $prop->pivot;
        }

        return view('index', compact('data', 'days', 'cities', 'city', 'id'));
    }

    public function selectedCity(Request $request)
    {
        $selected = $request->selected;

        $cities = City::get();

        $days = Day::get();
        return view('index', compact('selected', 'cities', 'days'));
    }


    public function changeCity(Request $request)
    {
        $data = Location::get('77.85.109.88');

        $cityName = $data->cityName;
        $cityId = DB::table('cities')->where('name', $cityName)->get()->toArray();
        $id = $cityId[0]->id;
        if (isset($request->selected)) {
            $id = $request->selected;
        }

        $tempType = 'C';
        if (isset($request->tempType)) {
            $tempType = $request->tempType;
        }

        $city = City::find($id);
        $days = Day::get();

        $data = [];
        foreach ($city->days as $prop) {
            $data[] = $prop->pivot;
        }

        $data[$city->id]['current_temp'] = ($tempType != 'C')
            ? $data[$city->id]['current_temp'] * 1.8 + 32
            : $data[$city->id]['current_temp'];
        $data[$city->id]['min_temp'] = ($tempType != 'C')
            ? $data[$city->id]['min_temp'] * 1.8 + 32
            : $data[$city->id]['min_temp'];
        $data[$city->id]['max_temp'] = ($tempType != 'C')
            ? $data[$city->id]['max_temp'] * 1.8 + 32
            : $data[$city->id]['max_temp'];
//        dd($data);
        return view('partial_index', compact('data', 'days', 'city', 'id','tempType'));
    }

    public function addFavourite(Request $request)
    {
        if (empty($request->city_id)) {
            return false;
        }
        $city_id = $request->city_id;
        return response('favourite')->cookie(
            'location_'.$city_id, 'cookie', 36000
        );
    }

    public function getFavourites()
    {
        $cookiesSelected = [];
        foreach ($_COOKIE as $cookieName => $cookieVal) {
            if (strpos($cookieName, 'location') === 0) {
                $cid = str_replace('location_', '', $cookieName);
                $cookiesSelected[$cid] = $cid;
            }
        }
        $cities = City::all();
        $favouritesCities = [];
        foreach ($cities as $city) {
            if (in_array($city->id, $cookiesSelected)) {
                $favouritesCities[$city->id] = $city->name;
            }
        }
        return response(json_encode($favouritesCities));
    }

    public function forgetCookie(Request $request)
    {
        $city_id = $request->city_id;
        Cookie::queue(Cookie::forget('location_'.$city_id));

        return response('forgetCookie');
    }

    public function changeScale(Request $request)
    {
        if (isset($request->value) && $request->value == 'C') {
            $temp = $request->temp;
        } else {

            $temp = $request->temp * 1.8 + 32;
        }
        return response($temp);
    }


}


