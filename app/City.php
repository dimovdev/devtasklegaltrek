<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded = [];

    public function days()
    {
        return $this->belongsToMany(Day::class)
            ->withPivot('min_temp','max_temp','precipitation','humidity','current_temp','type','wind');

    }
}
