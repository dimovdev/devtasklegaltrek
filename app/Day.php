<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    protected $guarded=[];

    public function cities()
    {
        return $this->belongsToMany(City::class)
        ->withPivot('min_temp','max_temp','precipitation','humidity','current_temp','type','wind');

    }
}
