<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/locale/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
});
Route::any('/','CityController@index')->name('index');
Route::post('add_favourite','CityController@addFavourite')->name('add_favourite');
Route::post('selected','CityController@selectedCity')->name('selected');
Route::post('/change_city','CityController@changeCity')->name('change_city');
Route::post('/get_favourite','CityController@getFavourites')->name('get_favourite');
Route::post('/forget_cookie','CityController@forgetCookie')->name('forget_cookie');
Route::post('/change_scale','CityController@changeScale')->name('change_scale');
